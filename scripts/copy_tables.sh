#!/bin/bash

cp -R ../proof_of_concept/tables/threshold parts/tables/

cp ../proof_of_concept/tables/distance_dist_kurtosis.tex parts/tables
cp ../proof_of_concept/tables/distance_dist_std.tex parts/tables
cp ../proof_of_concept/tables/igraph_comparison.tex parts/tables
cp ../proof_of_concept/tables/test_graphs_sizes.tex parts/tables
cp ../proof_of_concept/tables/test_graphs_speedup.tex parts/tables
