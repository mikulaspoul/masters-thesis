#!/bin/bash

cp ../proof_of_concept/figures/1core_speedup_ba-graph.pdf figures/
cp ../proof_of_concept/figures/1core_speedup_cond-mat-2003.pdf figures/

# degree dist
cp ../proof_of_concept/figures/degree_dist/* figures/degree_distributions/

# unweighted sampling
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_unweighted_ru.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_unweighted_ou.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_unweighted_rp.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_unweighted_ru.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_unweighted_rp.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_unweighted_ou.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_unweighted_ru.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_unweighted_ou.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_unweighted_rp.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_er-graph_unweighted_ru.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_er-graph_unweighted_ou.pdf figures/sampling/unweighted/
cp ../proof_of_concept/figures/sampling/absolute_er-graph_unweighted_rp.pdf figures/sampling/unweighted/

# weighted sampling normal
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_normal_ru.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_normal_ou.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_normal_rp.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_normal_ru.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_normal_ou.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_normal_rp.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_normal_ru.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_normal_ou.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_normal_rp.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_normal_ru.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_normal_ou.pdf figures/sampling/weighted/normal
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_normal_rp.pdf figures/sampling/weighted/normal

# weighted sampling uniform
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_uniform_ru.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_uniform_ou.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_uniform_rp.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_uniform_ru.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_uniform_ou.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_uniform_rp.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_uniform_ru.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_uniform_ou.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_uniform_rp.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_uniform_ru.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_uniform_ou.pdf figures/sampling/weighted/uniform
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_uniform_rp.pdf figures/sampling/weighted/uniform

# weighted sampling unit
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_unit_ru.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_unit_ou.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_as-22july06_weighted_unit_rp.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_unit_ru.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_unit_ou.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_ba-graph_weighted_unit_rp.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_unit_ru.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_unit_ou.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_cond-mat-2003_weighted_unit_rp.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_unit_ru.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_unit_ou.pdf figures/sampling/weighted/unit
cp ../proof_of_concept/figures/sampling/absolute_er-graph_weighted_unit_rp.pdf figures/sampling/weighted/unit

# counted and others
cp -R ../proof_of_concept/figures/sampling/counted figures/sampling/
cp -R ../proof_of_concept/figures/sampling_speed figures/
cp -R ../proof_of_concept/figures/distance_dist figures/
