import re
from pathlib import Path
from pprint import pprint

complete_text = ""

for file in Path("parts/").glob("**/*.tex"):
    complete_text += file.read_text()

acronyms_used_regex = re.compile(r"\\gls{([A-Z]+)}")

acronyms_used = set()

for match in re.finditer(acronyms_used_regex, complete_text):
    acronyms_used.add(match.groups()[0])

acronyms_defined_regex = re.compile(r"\\newacronym{([A-Z]+)}")

acronyms_defined = set()

for match in re.finditer(acronyms_defined_regex, Path("acronyms_list.tex").read_text()):

    acronyms_defined.add(match.groups()[0])

print("Defined")
print(acronyms_defined)
print("Used")
print(acronyms_used)
print("Undefined")
pprint(acronyms_used - acronyms_defined)
print("Defined, not used")
pprint(acronyms_defined - acronyms_used)
