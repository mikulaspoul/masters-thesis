PARTS_FILES = $(shell find parts -type f -name '*.tex')
DIAGRAMS_FILES = $(shell find diagrams -type f -name '*.tex')
FIGURES_FILES = $(shell find figures -type f -name '*.pdf')
CLASS_FILES = $(shell find class)

all: report.pdf

open: report.pdf
	gio open report.pdf >/dev/null 2>&1

clean:
	make -C diagrams clean
	ls -1 report.* | grep -v tex | xargs rm -f
	rm -f preamble.aux
	rm -f `find parts -type f -name '*.aux'`

report.pdf: *.tex biblio.bib $(PARTS_FILES) $(CLASS_FILES) $(DIAGRAMS_FILES) $(FIGURES_FILES)
	make -C diagrams all
	arara report.tex
	rm -f `find parts -type f -name '*.aux'`

.PHONY: count
count:
	texcount *.tex $(PARTS_FILES)

.PHONY: copy_figures
copy_figures: scripts/copy_figures.sh
	scripts/copy_figures.sh

.PHONY: copy_tables
copy_tables: scripts/copy_tables.sh
	scripts/copy_tables.sh

find_acronyms:
	grep -hEo '[[:upper:]]{3,}' $(PARTS_FILES) | sort | uniq

undefined_acronyms:
	python scripts/undefined_acronyms.py

undefined_citations:
	python scripts/undefined_citations.py

undefined_refs:
	python scripts/undefined_refs.py

sort_acronyms:
	sort acronyms_list.tex -o acronyms_list.tex
