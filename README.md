Fast calculation of the average path length in large complex graphs
===================================================================

Repo for the text of my Master's Thesis. The name is not final.

See LICENSE for license information.

Output: [https://masters-thesis.mikulaspoul.cz/](https://masters-thesis.mikulaspoul.cz/)
