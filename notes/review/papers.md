# Papers

 * !! Diameter of the world wide web
   * https://arxiv.org/pdf/cond-mat/9907038
   * Crawler which searched the web and recorded P_in(k) and P_out(k), resulting in two power-law distributions.
   * Created a random graph with the same degree distribution, analysing properties like average path length and the diameter as a function of size of the network. 
 * Sampling networks from their posterior predictive distribution
   * Could be useful for sampling as a possibility to estimate parameters
   * https://www.cambridge.org/core/journals/network-science/article/sampling-networks-from-their-posterior-predictive-distribution/95BFC11F8A4948122A6211AD10A2017E
   * More of a method of generating networks with more constraints on some properties, like mixing and clustering. The initial constraints can be done by sampling from the actual graph, this method is for generating random graph in the same "category" with similar properties, without observing more of the original network.
 * Network Sampling and Model Fitting
   * Again, for the sampling, discussion on how is it being sampled
   * https://www.cambridge.org/core/books/models-and-methods-in-social-network-analysis/network-sampling-and-model-fitting/A36DB23FBFC0EB899482D9244582AF1E  
   * A description of probabilistic sampling and how it recreates original graph, mostly in surveys
 * Modeling social networks from sampled data.
   * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4637981/
   * Overview of multiple sampling methods for both directed and undirected networks (ego-centric, link-tracing), with application on a social network of lawayers
 * !! Large-scale topological and dynamical properties of the internet
   * https://pdfs.semanticscholar.org/1a0e/d5aac8783821a2c23aa7f8bb0bbfb0bcf4e3.pdf
   * A study of the AS graph in time, found that the parameters aren't changing much even though the graph is growing quite quickly.
   * Contains a nice explanation of the hop-plot, does measure average path length and notes that it is smaller than one would expect, thanks to small world.
   * Tried to recreate the graph with multiple randon-graph generators, found that it is hard to reproduce directly, while Molloy and Reed was the best.
   * Absolutely annoyingly the citation style is terrible and only mentions the authors and barely a resemblance of any link to what it is and where you can find it...
 * !! ANF: A Fast and Scalable Tool for Data Mining in Massive Graphs
   * http://www.cs.cmu.edu/~christos/PUBLICATIONS/kdd02-anf.pdf
   * Stands for Approximate Neighbourhood Function, a scalable method for compution graph similarity, subgraph similarity and vertex importance
   * Compares running time to RI approximation and random sampling of a percentage, apparently random sampling is unscalable, but the method, it appears has a constant sample percentage, I was thinking the percentage can be descreased with size
   * For random sampling to approx the NF, the time complexity rises quadratically
   * The ANF method uses bitwise magic to approximate the NF/hop-plot, in such a way that the algo is very scalable and paralizable, even for graphs stored on disk (prevents random access issues)
   * It introduces/uses a hop-plot exponent for comparing multiple graphs
   * It compares the method to other approximations of the NF, finds it is much faster and much more accurate
 * HyperANF: Approximating the Neighbourhood Function of Very Large Graphs on a Budget
   * http://wwwconference.org/proceedings/www2011/proceedings/p625.pdf
   * Using a slightly different method speeds up the processing massively
   * Billions of links and still fast even on a small server
   * Does approximate average distance
 * Fast Approximation of Betweenness Centrality through Sampling - "VC-hell"
   * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.700.9899&rep=rep1&type=pdf
   * Sampling with bounds proved thanks to VC dimension crazyness, probably approximately correct, sample size independent of the size of the graph
   * Sample size only dependant on "vertex-diameter", a maximum number of vertices in a shortest path used for the algo
 * Better Approximation of Betweenness Centrality
   * http://algo2.iti.kit.edu/schultes/hwy/betweennessFull.pdf
   * Actually slower than the VC-hell paper
   * Uses pivots as the source for single-source-shortest-path, but makes the influence on nodes close to the pivots smaller
 * Shortest path tree sampling for landmark selection in large networks 
   * https://academic.oup.com/comnet/article/5/5/795/3865491
   * Not very relevant to topic, dicusses selection of landmarks (almost pivots) for estimating distance in large online graphs
 * Topology manipulations for speeding betweenness centrality computation 
   * https://academic.oup.com/comnet/article/3/1/84/490896#7499042
   * Not very relevant, speeds up the exact calculation quite a lot with reducing the a graph so duplicate nodes (nodes which serve the same function, e.g. just multiple choices) are reduced to one
 * Fast estimation of diameter and shortest path - "ACIM"
   * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.53.8313&rep=rep1&type=pdf
   * A very theory-heavy paper about using the dominating set to get a approximation of the dimater and the shortest paths.
   * Everything proven, the estimate is within 2/3 of the actual dimater, complexity is O(m * sqrt(n log m) + n * n log n))
   * In general the approximation is still quite expensive, it doesn't actually seem that expensive
   * No empirical value
 * Fast approximation algorithms for the diameter and radius of sparse graphs
   * http://theory.stanford.edu/~virgi/diam.pdf
   * Another theory-heavy paper, improvement of ACIM, ANF/HyperANF seems faster
 * Better approximation algorithms for the graph diameter
   * Theoretical improvement of the ACIM approach   
 * Faster Approximation of Distances in Graphs
   * https://link.springer.com/chapter/10.1007/978-3-540-73951-7_47
   * Similar results, n^2 approximation of all pairs
 * Fast and simple approximation of the diameter and radius of a graph.
   * Boitmanis K, Freivalds K, Ledinš P, Opmanis R 2006
   * Additive error O(sqrt(n)) in O(m sqrt(n)) time, the algorithm which repeats BFS and restarts from the furthest node, repeats for k times, k can be set to k=sqrt(n)
   * Does actually quite good, but the result is only shown on graphs up to about 5000 nodes, error better than "Power of BFS" paper algorithms
   * Could be much worse on larger graphs
 * Computing the Eccentricity Distribution of Large Graphs
   * https://www.mdpi.com/1999-4893/6/1/100/pdf
   * A very good paper, about the eccentricity distribution
   * Very effective way to calculate both the exact and approximate distribution of the radius, average eccentricity and diameter
   * The speedup is so good and the approximation is so nice that approximating the radius and diameter is not worth it in my thesis
   * Together with ANF a must-cite thing
   * Speed-up made possible by pruning and bounds
   * Perhaps interesting citations
     * [5-7] - efficient algorithms for the diameter
     * [12-13] - effective radius/diameter
 * Sampling from Large Graphs
   * https://cs.stanford.edu/people/jure/pubs/sampling-kdd06.pdf
   * A study of multiple sampling strategies on a couple of properties, not including average path length
   * Finds that forest fire burning sampling performs best on those properties
   * About 15% sample is needed
 * Fast approximation of average shortest path length of directed BA networks
   * Approximation on scale-free networks
   * Using Global Reachable Nodes, a node which can reach a large portion of other nodes
   * With growing size the portion of nodes to get error < 0.5 is smaller and smaller
 * Fast Shortest Path Distance Estimation in Large Networks
   * http://chato.cl/papers/potamias_2009_fast_shortest_path.pdf 
   * Estimating distance using landmarks, a study of selecting the landmarks
 * Distance Distribution and Average Shortest Path Length Estimation in Real-World Networks
   * https://link.springer.com/chapter/10.1007/978-3-642-17316-5_32
   * Introduces a random-vertex sampling, which I was thinking I would use
 * Conservation of alternative paths as a method to simplify large networks
   * https://dlnext.acm.org/doi/pdf/10.1145/1610304.1610305
   * A reduction of nodes into super-nodes which play the same role in topology of cycles/triangle
   * Nodes are merged into one if they're on a tree structure
   * Quite a big reduction in the number of nodes
 * Distributed estimation of diameter, radius and eccentricities in anonymous networks*
   * https://www.sciencedirect.com/science/article/pii/S1474667015348023
   * Only interesting for the anonymous aspect

## Still to read
 * On the Power of BFS to Determine a Graph’s Diameter.
   * Breadth First Search (BFS), Last Layer Minimum Degree (LL+) and Lexico-graphic Breadth First Search (LBFS), squared complexity
   *  D.G. Corneil, F.F. Dragan, and E. Kohler
 * All Pairs Almost Shortest Paths
   * D. Dor, S. Halperin, and U. Zwick. 
 * Minimal and maximal characteristic path lengths in connected sociomatrices
   * https://www.sciencedirect.com/science/article/pii/S0378873303000224
 * Estimating network structure via random sampling: Cognitive social structures and the adaptive threshold method
   * https://www.sciencedirect.com/science/article/pii/S0378873312000408
 * Estimating network properties from snowball sampled data
   * https://www.sciencedirect.com/science/article/pii/S0378873312000548
 * Data Mining on Social Interaction Networks
   * https://jdmdh.episciences.org/11
 * On the Emergence of Shortest Paths by Reinforced Random Walks
   * https://ieeexplore.ieee.org/document/7593357
 * Approximating Clustering Coefficient and Transitivity
   * http://jgaa.info/getPaper?id=108
 * On Sampling Nodes in a Network
   * http://gdac.uqam.ca/WWW2016-Proceedings/proceedings/p471.pdf
 * Dynamic and Historical Shortest-Path Distance Queries on Large Evolving Networks by Pruned Landmark Labeling
   * http://wwwconference.org/proceedings/www2014/proceedings/p237.pdf
 * Estimating Clustering Coefficients and Size of Social Networks via Random Walk
   * http://www2013.w3c.br/proceedings/p539.pdf
 * Estimating Sizes of Social Networks via Biased Sampling
   * http://wwwconference.org/proceedings/www2011/proceedings/p597.pdf
 * Estimating topological properties of weighted networks from limited information
   * https://arxiv.org/abs/1409.6193
 * On the Complexity of Sampling VerticesUniformly from a Graph
   * http://drops.dagstuhl.de/opus/volltexte/2018/9153/pdf/LIPIcs-ICALP-2018-149.pdf
 * Average Distance Queries through WeightedSamples in Graphs and Metric Spaces: HighScalability with Tight Statistical Guarantees
   * http://drops.dagstuhl.de/opus/volltexte/2015/5329/pdf/39.pdf
 * Efficient Diameter Approximation for LargeGraphs in MapReduce
   * (presentation)
   * http://materials.dagstuhl.de/files/17/17141/17141.GeppinoPucci.Slides.pdf
 * On approximating the longest path in a graph
   * https://link.springer.com/chapter/10.1007/3-540-57155-8_267
 * On Power-Law Relationships of the Internet Topology 
   * Should include something about distribution of shortest path lengths, also maybe the first introduction of a hop-plot?
   * https://dl.acm.org/citation.cfm?id=316229
 * Estimating the size of generalized transitive closures
   * Lipton/Naughton
   * Apparently unscalable (when not using data in-memory), bad performance even with 15% of the data
 * Size-estimation framework with applications to transitive closure and reachability.
   * Cohen
   * Apparently only approximation of the NF before the ANF paper
