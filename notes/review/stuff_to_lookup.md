# Related stuff to lookup/research

 * Commute Time Distance
   * NetworKit Can do approximation
 * Effective Diameter
   * "Effective diameter is defined as the number of edges on average to reach a given ratio of all other nodes."
   * Again, NetworKit can do approximation, based on paper "A Fast and Scalable Tool for Data Mining in Massive Graphs"
 * Practical Diameter
   * mentioned in ANF paper without citation
 * Hop-Plot
   * NetworKit, Same Paper
   * "Computes an approxmation of the hop-plot of a given graph. The hop-plot is the set of pairs (d, g(d)) for each natural number d and where g(d) is the fraction of connected node pairs whose shortest connecting path has length at most d."
   * "Large-scale topological and dynamical properties of the internet" provides a nicer explanation
   * Basically a function of number of hops that says which percentage of nodes can be reached in that many nodes
   * "basic neighbourhood function"
   * Hop-exponent - seems that the distribution of the hop is often power-law distributed [ANF]
     * Similar graphs have similar hop-exponents
 * Other approximations from NetworKit (e.g. betweeness)
 * Characteristic path length
   * Different name for average path length
 * Bridge graphs
   * Should be apparently hard for diameter algorithms

