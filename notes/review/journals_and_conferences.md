# Stuff for literature review

## Journals on Network Science

 * Cambridge Network Science 
   * https://www.cambridge.org/core/journals/network-science
 * Oxford Journal of Complex Networks
   * https://academic.oup.com/comnet
 * Applied Network Science 
   * https://appliednetsci.springeropen.com/
 * International Journal of Network Science
   * https://www.inderscience.com/jhome.php?jcode=ijns
 * Journal on Chain and Network Science
   * https://www.wageningenacademic.com/loi/jcns
 * Social Networks
   * https://www.journals.elsevier.com/social-networks/
 * Journal of Data Mining & Digital Humanities
   * https://jdmdh.episciences.org/browse/latest
 * IEEE Transactions on Network Science and Engineering
   * https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=6488902
 * IEEE Transactions on Computational Social Systems
   * https://ieeexplore.ieee.org/xpl/RecentIssue.jsp?punumber=6570650
 * Journal of Graph Algorithms and Applications
   * http://jgaa.info/
 * Computational Social Networks
   * https://computationalsocialnetworks.springeropen.com/

## Conferences
 * https://sites.google.com/site/cxnets/conferences
 * http://www.cxnets.org/conferences.html
 * http://vermontcomplexsystems.org/events/netsci/
 * Dagstuhl Seminar on Graph Algorithms and Applications
 * International Workshop on Algorithms and Data Structures
 * Annual Workshop on Algorithms and Computation

## Other sources
 * http://www.sociopatterns.org/publications/
