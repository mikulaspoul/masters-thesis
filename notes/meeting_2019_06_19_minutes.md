# Meeting on the 28th of May 2019 minutes

 * community structure will have a big impact probably
 * focus on connected and giant component
 * sampling - exploring the graph
   * focus on the avg path length property being the same for full graph as for the sampled graph
   * using a portion of the graph to get the approximation of full picture
   * examine community with the sampling is the challenge, with a small sample
   * community
     * proportionate to size
     * the larger community the more nodes in that sample
     * some hybrid sampling probably
   * being able to say at least X% of the full network is needed to get a accurate approximation
   * BOSAM might be useful to visualise the structure in the sample and full
   * use visualizations
 * graphs:
   * different types (www/social/bio)
   * but also focus on more of the same type
   * how well it predicts + standard deviation
 * source node selection
 * reach all nodes in the BFS? does it matter that uch?
 * formula for the sampling percentage?
