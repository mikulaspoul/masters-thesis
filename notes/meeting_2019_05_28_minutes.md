# Meeting on the 28th of May 2019 minutes

 * Justification
   * Internalize and explain very clearly why this is useful
   * Why do we need to do it, can't we just simply calculate the full value
 * Validation
   * It might be better to restrict the validation to a couple of problems, showing it works on all types of graphs is not needed, because one can say it works good on this type of network, with similar approaches one can apply to other graphs
 * Sampling
   * Method of sampling source nodes and calculate distances to all other nodes is valid
   * Another method might be just to simply take a sub-graph and calculate on it ("partial view")
     * Much more real-world, because one can sometimes not see the full graph, or just can't in any sense store it all (major example being the web)
 * Type of approach preferred? 
   * Top Down - develop the theory first and then test it
   * Bottom Up - experiment with several things and then construct theory around it
   * It should be one of them, top down is more theory
 * Comparing networks idea:
   * The minimum number of rewires needed to make the graphs the same
   * Hard to prove it is the minimum
 * Q&A:
   * Min 30, about 50, max 80 pages
   * LaTeX template is nonbinding
   * Citation style doesn't matter, both prefer [1] in order
 * Size of networks:
   * About 50k is enough, up to 200k let's say
   * Wikipedia in Complex Networks project was around 300k
 * Review
   * Journals, Conferences
   * Look in sociology, psychology stuff
   * IEEE journals
 * Metric
   * "Average Diameter" is good idea, very explainable and useful to describe the size of the network
   * Give standard deviation of multiple runs of the approximation
 * Software
   * Better as a function directly in graph-tool, then a separate tool
   * UCL Registry - UCL sanctioned software repository
 * Cloud
   * it is not uncommon for custom software to be installed
   * email Zhou/TST with queries
   * CPUs should not be a problem, RAM is bit more restricted, should be fine for this
 * Next meeting
   * 21st of June, 16:30
 * Prepare a document with summary
   * What and why, why is it important/useful
   * 2-3 pages
   * Including some lit review
 * Grading
   * Depends mostly on the second grader
   * Don't use terms used in other fields much, explain what graph theory things are
   * General CS language
