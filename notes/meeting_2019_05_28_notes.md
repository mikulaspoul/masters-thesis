# Meeting on the 28th of May 2019

## Metrics I want to focus on

In general regarding the distribution of shortest distances. 

 * Average shortest distance
 * Radius
 * Diameter
 
New metric - "average diameter" and "average radius", a confidence interval for the metrics

  * e.g. 95% of distances are shorter than XX
  * e.g. 95% of distances are longer than XX
  * configurable
  * should be easier to approximate 

## Presumed Layout

 * Introduction
 * Detailed description of the problem
   * For comparison of large graphs the precise values are not necessary
   * Finding the metrics is complex
 * Literature review
   * Approximation methods for the metrics
   * Approximation methods for other metrics
 * Approximation methods description
   * Definition of the approximation methods
   * The new metric description and its approximation 
 * Implementation
   * Analysis of existing Python tool
     * I am pretty much decided I want to use graph_tool, but think I should describe why.
     * https://graph-tool.skewed.de/performance - a nice clear evidence of its superiority in speed
     * it has a distance histogram function which can sample, not average/radius/distance etc
   * Description of the implementation
     * First a function for calculating the precise metric for unweighted graphs
     * Then for weighted graphs
     * Finally the approximation methods
 * Performance analysis
   * Generate a couple of large random graphs (in increasing size)
     * ER, Small World, BA
   * Collect a couple of real-world large graphs
     * The tool has a couple of the packed (citations, AS, PGP trust, Enron emails)
     * Wikipedia?
   * Get the full metrics
   * Run the approximating metrics, compare results
   * Provide a heuristic for how much should be sampled
 * Conclusion
 
 
## Questions

  * Page limit/Word limit
  * Cloud availability
  * Next meeting (19th to 25th of June)
  * Format of the thesis text - follow the template provided?
  * Citation style (numbers vs shortcuts)
