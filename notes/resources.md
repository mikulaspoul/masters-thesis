# General resources

http://www.network-science.org/
https://www.barabasilab.com/
http://networksciencebook.com/

## General referencing

 * The structure and function of complex networks
   * A comprehensive 50 page review of complex networks, maybe a good source for citing stuff
   * https://arxiv.org/pdf/cond-mat/0303516.pdf
 * Complex networks: Structure and dynamics
   * Even longer review 
   * https://www.sciencedirect.com/science/article/pii/S037015730500462X (Access through UCL)
 * SNAP
   * A large collection of graphs
   * https://snap.stanford.edu/data/index.html
 * Exact and Approximate Distances in Graphs — A Survey
   * Theoretical overview of distance calculations
   * https://link.springer.com/chapter/10.1007/3-540-44676-1_3
