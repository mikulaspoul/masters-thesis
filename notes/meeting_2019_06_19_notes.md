# Meeting on the 28th of May 2019

## Update
  * Collapsing node with degree 2 (inspired by the paper sent)
  * Finally compiled the tool, gonna code a C++ extension

## Questions

  * OK to focus just on strongly connected components on undirected networks? Isn't that too short of a topic?
  * Conservation of alternative paths as a method to simplify large networks follow-up paper?
    * It said it will follow-up with application, couldn't find it.
  * Is stochastic block model scale free?
  * How to measure community structure? Through modularity? Or how much of the network is populated by it? 
  * How to better find that this hasn't been done? Don't want to repeat situation that it was done before like with my previous version of the topic...
