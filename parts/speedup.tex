\chapter{Faster exact calculation}\label{ch:speedup}
\glsresetall

This first part focuses on speeding up the calculation of the exact \gls{APL} in large graphs.
The average path length is one of the most common properties used to describe a graph.
It is a simple and explainable measure of efficiency and speed of communication within a graph.

The trouble is that with the growing size of a network calculating the average path length gets harder, because of the complexities of algorithms for finding the pair-wise distance between all vertices, as all the distances are needed for the exact value.
This is especially the case for weighted graphs, where the algorithms are comparatively slower to unweighted graphs, as is discussed in detail in section~\ref{sec:speedup:analysis}.
Large graphs are very common these days, in the age of constant data collection, and once the number of vertices reaches a certain point, the time to calculate the distances becomes prohibitive.

Many of the real-world graphs are also constantly changing, especially in the case of social networks.
This means that if somebody wants to track the changes in the average path length, the value has to be calculated in all the stages of the collected temporal graph.
This, depending on the size of the network and the frequency of the data collection, can make the calculation very slow.
Any speedup of the calculation on a single graph then makes the speedup on temporal graphs even larger in terms of the absolute amount of time.

Any real-time analysis of these dynamic graphs is also not possible due to the complexity of the problem.
For all of these reasons, any speedup to the calculation is useful.

Many of the real-world graphs are so-called \textit{scale-free}, meaning they follow a power-law degree distribution.
These scale-free graphs include social networks, web networks and many other types of real-world graphs~\cite{barabasi1999emergence}.
A scale-free graph, thanks to its distribution, has a large portion of vertices with a low degree.
From some of these vertices, the distances to other vertices can be much more easily calculated than through the classical algorithms, so they can be dropped from the expensive pair-wise distance calculations, leading to a speedup.
This is the case for all vertices with degree one and two, and depending on the graph some vertices with a slightly higher degree can be excluded as well.

For this thesis, only undirected graphs and the largest connected components are considered.
The approach could be updated to work on directed graphs, similarly also across multiple components, but it would add unnecessary complexity and edge cases for this thesis.

\input{parts/speedup/analysis.tex}
\input{parts/speedup/implementation.tex}
\input{parts/speedup/results.tex}
\input{parts/speedup/further.tex}
