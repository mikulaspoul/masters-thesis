\subsection{1-core simplification of a graph}\label{subsec:speedup:analysis:1core}

The large number of vertices with degree one can be used to simplify the pair-wise distance calculation and in that speeding it up.
Let's take a portion of a graph shown in figure~\ref{fig:1core}, with dotted lines from vertices $a$ and $c$ indicating a connection to more vertices in the rest of the graph.
The figure shows that any path from $e$ to all other vertices passes through vertex $b$.
Its distance to any other vertex $x$ is $w_{e, b} + d_{b, x}$.
One can then remove the vertex from the graph for the pair-wise distance calculation and then just take it in account in the summing of distances.

The situation is similar with vertices marked with an orange border, $f$ to $j$, any path to all other vertices passes through vertex $b$, with the exception to other vertices marked with the same colour.
Distance to the other vertices is therefore the sum of distance to $b$ from both ends, for example for $i$ the distance to a random $x$ is $d_{i, b} + d_{b, x}$, and the distance to a vertex $x$ with the same colour is simply $d_{i, x}$.

Note that vertices with a degree more than one and two can be included in this, take vertex $g$, which has degree 4.

\begin{figure}[htp]
    \centering
    \includegraphics[width=0.6\textwidth]{diagrams/1core.pdf}
    \captionsetup{justification=centering}

    \caption{Illustration of a 1-core simplification of a graph}
    \label{fig:1core}
\end{figure}

This simplification is inspired by the 2013 paper \articlename{Computing the eccentricity distribution of large graphs} by Takes and Kosters~\cite{takes2013computing}.
This paper focuses on efficiently computing the eccentricity distribution (and the radius and diameter).
When calculating the eccentricity one is only interested in the longest path, not all the paths, so they reduced multiple one-degree vertices neighbouring a single vertex to one, as that retains the maximum distance.
I was inspired by this to remove all the vertices with degree one with the update to the sum of distances and realised that actually, the simplification can continue further.

\subsubsection{Definitions}

Let's describe these concepts more precisely.
This reduction can be performed on the \textit{1-core} of the graph.
A 1-core is a set of vertices obtained by iteratively removing all vertices from the graph with degree 1 until all other vertices have a degree higher than 1, the 1-core being the removed vertices.
This is a specific case of a \textit{k-core}, where this can be done for any degree~\cite{seidman1983network}.
When an induced graph with only the vertices from the 1-core is created, it is a forest, a graph comprised of multiple trees.

Let's define some notation for vertices in the 1-core and their properties.
First, let $V^1$ denote the set of vertices in the 1-core and $n_1$ the size of this set.
Let $p_i$ denote the closest vertex outside the 1-core to which there exists a path from $i$ and $d_i$ the distance to that vertex.
Let $t_i$ denote be the identifier of the tree on which $i$ appears because as shown in the example figure, multiple trees can be connected to the same vertex outside the 1-core.
Additionally, let $V^t_a$ be the set of vertices with $a$ being their tree identifier and $T$ be set of tree identifiers.

\subsubsection{Algorithm for the average path length calculation}

Algorithm~\ref{al:1core_ad} shows the algorithm for summing of pair-wise distances with the 1-core simplification.
For a vertex outside the 1-core, the sum of distances from that vertex is the sum of distances to other vertices outside the 1-core plus the distances to vertices in the 1-core (via their closest vertex).
For a vertex in the 1-core, this can be simplified as the sum is linked to the sum of distances of its closest vertex.
It is the sum of the distances from the closest vertex to all vertices, with the distances from the closest vertex to vertices on the tree being replaced with distances just within the tree, plus the distance to the closest vertex times the size of vertices outside the current tree.

\begin{algorithm}[htp]
    \input{parts/algos/1core_ad.tex}
    \caption{Summing of pair-wise distances after 1-core reduction}\label{al:1core_ad}
\end{algorithm}

\subsubsection{Detecting the 1-core}

With this updated algorithm the pair-wise distance has to be calculated only on the induced subgraph with vertices $V \setminus V^1$ and on induced subgraphs of individual trees.
First, though, the 1-core has to be detected, showed in algorithm~\ref{al:1core_det}.

\begin{algorithm}[htp]
    \input{parts/algos/1core_det.tex}
    \caption{Detecting the 1-core, distances to closest vertices and tree identifiers}\label{al:1core_det}
\end{algorithm}

The 1-core detection is based on the algorithm for detecting the k-core value by Batagelj and Zaveršnik~\cite{batagelj2011fast}, modified to just detect the 1-core.
The algorithm first works on an iterative basis, first processing vertices with degree 1, then vertices that would have a degree 1 if the previous were removed and so on while there are vertices to process.
The neighbour not yet in the set $V^1$ is marked as the temporary closest vertex.

Once all the vertices in the 1-core are found, they are iterated over in the reverse order they were found in, and the distance from the closest vertex is calculated and the tree identifier set.
The tree identifier is the vertex from a tree that is directly connected to the closest vertex.
Since the order of finding vertices in the 1-core is from the furthers from the closest vertex of the tree, the distance and tree identifier can be passed through as a message.
The final closest vertex of each edge is the temporary closest vertex of the vertex set as tree identifier.

\subsubsection{Complexity}

Let's recall that the complexity of finding the average path length with the basic approach is $\bigO(n^2 + nm + n^2 \log n)$.
When using the 1-core simplification the complexity has a couple of more terms, but it is better if the 1-core is significant.

For simplicity let's define $n_r = n - n_1$ and $m_r = m - n_1$, as the number of vertices and edges in the remaining part of the graph.
The number of edges removed with the 1-core equals the number of vertices in the 1-core.
This is due to a tree having exactly one less edge than the number of vertices, but each of these trees has to be connected to the rest of the graph.

Finding of the 1-core has a complexity of $\bigO(n + n_1)$, $n$ because of the initial pass through and the rest is the loops over the vertices of the 1-core.
The complexity of the sum of distances depends on how many individual trees are in the 1-core, in the worst case all of 1-core would be in one big tree and the complexity would be $\bigO(n \cdot n_r + n_1^2)$.
It would be unusual, but the complexity is still better.
And then pair-wise distances, for the main component $\bigO(n_r m_r + n_r^2 \log n_r)$ and then for individual trees, in the worst case $\bigO(n_1^2 + n_1^2 \log n_1)$.
So the final complexity in the worst case would be $\bigO(n_r^2 \log n_r + n_1^2 (2 + \log n_1) + n_r m_r + n (1 + n_r) + n_1)$.

That is the worst-case scenario, in real graphs the 1-core will not be one big tree, it will be many small ones, actually, most of them would be single vertices.
That adds a little more improvement to the complexity.
Let's say that $t$ is the average tree size, then the complexity of the sum is $\bigO(n \cdot n_r + n_1 t)$, and the complexity of finding pair-wise distances in the trees is $\bigO(|T| \cdot t^2 \log t)$.
In the four test graphs used later in the chapter, the average tree size is between $1$ and $1.2$, which makes the complexity better, but the most time-complex part is still the complexity of Johnson's algorithm on the remaining part of the graph.
In other graphs I have examined most of the 1-core is just single vertices, just as in the four test graphs.

Let's also consider for a moment the complexity when using multiple \glspl{BFS}, originally with complexity $\bigO(n^2 + n (n + m))$.
The complexity of the updated algorithm is $\bigO((n + n_1) + (n \cdot n_r + n_1^2) + (n_r (n_r + m_r)) + (n_1 (n_1 + n_1)))$ which equals to $\bigO(n_r (n_r + m_r) + n_1^2 + n (n_r + 1) + n_1 )$.
Still an improvement, but relatively a smaller one compared to the improvement while using Johnson's.

This algorithm also has a better memory complexity, the original having a $\bigO(n^2)$ requirement, this improved has $\bigO(n_r^2 + n_1^2 + n_1)$.
The algorithm allows for parallelisation.
