\subsection{2-chains simplification of a graph}\label{subsec:speedup:analysis:2chain}

After the 1-core is removed, there are still many vertices with degree two remaining in the rest of the graph, and this can include vertices with degree originally higher thanks to a connection to the 1-core tree.
These can be used to further speed up the calculation.

Figure~\ref{fig:2chain} contains illustration of the general idea.
First a graph after removing 1-core vertices in figure~\ref{fig:2chain_original}.
Vertices with degree 2 are marked with a green border, other with a blue border.
For example vertex $m$, a path to any other vertex must go through either $a$ or $c$, and the distance to an other vertex $x$ is $\min(d_{a,x} + w_{a, m}, d_{c, x} + w_{c, m})$.
This works even for multiple linked vertices of degree~2, e.g.\ vertices $n$ and $o$.
For them the distance to other vertices is again either through $a$ or $c$, but with the distance to an other vertex $x$ is $\min(d_{a,x} + d_{a, m}, d_{c, x} + d_{c, m})$ instead.
These linked vertices of degree 2 will be further referred to as \textit{2-chains}.

Therefore the vertices in 2-chains can be removed from the graph and instead replaced by a new edge with weight being the sum of weights of edges being removed.
In case there are multiple 2-chains between the same vertices, only one edge can be created with the minimum weight.
The result of this reduction is shown in figure~\ref{fig:2chain_reduced}, new edges are marked with green with their weights listed (with the weights of the original edges being all ones).

\begin{figure}[htp]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.5\linewidth]{diagrams/2chain.pdf}
  \caption{The original graph}
  \label{fig:2chain_original}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.5\linewidth]{diagrams/2chain_reduced.pdf}
  \caption{The reduced graph}
  \label{fig:2chain_reduced}
\end{subfigure}
\caption{Illustration of a 2-chain simplification of a graph}
\label{fig:2chain}
\end{figure}

This idea is inspired by a 2009 paper \articlename{Conservation of alternative paths as a method to simplify large networks} by Liu and Mondragón~\cite{liu2009conservation}, which focused on the simplification of graphs while retaining the same number of alternative paths.
This does not preserve the number alternative paths (even shown in the example, one alternative path is lost in the reduction between $a$ and $c$), but it does preserve distances between the remaining vertices and connectivity.

\subsubsection{Definitions}

Let's again define things precisely, on a graph that's already been simplified by removing the 1-core.
The following specification works for undirected graphs, regardless of having weights.
If the graph does not have weights, unit weights are assigned to each edge.

Let \textit{2-chain} be a term for a set of connected vertices with degree two, and let $V^2_i$ denote that set, $i$ being the chain identifier and $C$ the set of chain identifiers.
Let $V^2$ denote the set of vertices in all 2-chains in a graph and $m_2$ size of that set.
The 2-chain always connects two vertices, lets call them \textit{closest vertices}, so let's say that for a vertex $v$, one of them is \textit{left}, denoted by $p^l_v$, and other is \textit{right}, denoted by $p^r_v$.
There are of course no directions in graphs, but let's use that term in this context.
All vertices from a single 2-chain will have the same left and right closest vertices.
For a vertex $v$, the distance to $p^l_v$ and $p^r_v$ is $d^l_v$ and $d^r_v$ respectively.
A property of all vertices in a chain having the same left and right closest vertices, the distance of two vertices $v$ and $w$ from the same chain is $|d^l_v - d^l_w|$ or $|d^r_v - d^r_w|$ (only works on positive weights, but others are not considered in this thesis).
Let $c_v$ be the chain identifier of vertex $v$, and finally, let's use $d^2_i$ for the distance of the chain $i$ from one closest vertex to the other.

\subsubsection{Algorithm for the average path length calculation}

Algorithm~\ref{al:2chain_ad} shows the algorithm for summing of pair-wise distances using the 2-chain simplification.
The distance from remaining vertices to vertices in 2-chains and vice-versa is the minimum of the path through one or the other closest vertex.
The distance from a vertex from a 2-chain to another vertex in a 2-chain is the minimum of distances through both closest vertices, so four options, for vertices on the same 2-chain also the path within the 2-chain must be considered.

\begin{algorithm}[htp]
    \input{parts/algos/2chain_ad.tex}
    \caption{Summing pair-wise of distances after 2-chain reduction}\label{al:2chain_ad}
\end{algorithm}

\subsubsection{Detecting the 2-chains}

Algorithm~\ref{al:2chain_det} shows detecting 2-chains and their properties.
First, all vertices with degree 2 are added to the set $V^2$, and then the neighbouring vertices are traversed until all vertices in $V^2$ are not assigned to a specific chain.
In the traversal in both directions, the closest vertices are also detected.
Once the closest vertices are detected, each chain is traversed in order and the distance is calculated by adding weights of the edges to each vertex from one side, then the chain is traversed back and distances are set from the other side.

\begin{algorithm}[htp]
    \input{parts/algos/2chain_det.tex}
    \caption{Detecting 2-chains, their closest vertices, distances to them, sorting into individually identified chains}\label{al:2chain_det}
\end{algorithm}

\subsubsection{Complexity}

The complexity of calculating the average path length, mainly the summing of all distances, is still $\bigO(n^2)$ just as in the naive approach.
The summing is two nested for loops over all vertices, with some extra operations, but asymptotically still $\bigO(n^2)$.

The detection of the 2-chains has time complexity of $\bigO(n + n_2)$, first the pass over all vertices and then several passes over all the chains to find the closest vertices and the distances.

The pair-wise distances have to be calculated on the rest of the graph, which has $n_r = n - n_2$ vertices, and $m_r = m - n_2$ edges.
Each chain has one more edge than the number of vertices on it, and one edge has to be created for each chain, in the worst-case scenario, if there is exactly one chain between any two sets of closest vertices, if there are multiple the reduction in edges is bigger.

Since this simplification always creates a weighted graph, it is unsuitable for previously unweighted graphs to use this approach, because the multiple BFS algorithm cannot be used.
The complexity of finding the pair-wise distance on the rest of the graph is then $\bigO(n_r m_r + n_r^2 \log n_r)$.

Overall complexity using just this approach is then $\bigO(n_r m_r + n_r^2 \log n_r + n^2 + n + n_2)$, compared to the original $\bigO(nm + n^2 \log n + n^2)$.
