\chapter{Introduction}\label{ch:intro}
\glsresetall

Graphs are a simple way of looking at naturally occurring phenomena in the real world, which provide many insights on how the researched objects interact.
A graph consists of two sets of objects, a set of vertices, and a set of edges, which connect these vertices (also called networks, nodes, and links respectively).
Many different properties can be calculated on a graph.

These days, with the emergence of social networks and improvements in data mining, many real-world graphs grow in size very rapidly.
The computational complexity of calculating the properties differs by property, some can be calculated quickly even on large graphs, but some others, not even very complicated properties, have prohibitively high complexity.

One of these basic properties is the \gls{APL} which is very often used in graph analysis to show the efficiency, speed of communication or propagation within the graph.
It is the average of all distances within a graph, so it shows what distance has to be traversed before reaching all other vertices on average.
The computational complexity of calculating the APL goes up quickly because the distances from all vertices to all other vertices need to be calculated, and the problem gets even worse on weighted graphs.

For any single graph, one might bear this, even though the calculation could take a couple of hours on a usual workstation, but once one needs to calculate the value for multiple different graphs, any possible speedup would save time and energy.
The time needed to calculate the value could be especially an issue for calculating the value on temporal graphs, special graphs which have a different set of edges at each step in time.
These could be for example created by storing the structure of a social network every day.
Any improvement in the time complexity of the calculation would then be multiplied by the number of steps in the temporal graph.

The time complexity also prohibits usage of the average path length in real-time applications for large graphs.
In this world, all social media sites are graphs and all technological networks running the world are graphs and the complexity prohibits the real-time calculation of the APL on the constantly evolving graphs.

In some cases, the exact value is not even strictly necessary if there is a fast and accurate approximation available.
In exploratory data analysis when one needs a quick overview of some graph's properties, to get a sense of the graph, letting a computer compute the exact value is a waste of both time and the electricity to run the computer.
Similarly, when for example comparing two graphs, exploring if they are similar, comparing approximations can be sufficient enough to determine the difference in values.

This thesis attempts to tackle both of these issues in the calculation of the average path length, the time complexity and the approximation.
First speeding up the calculation of the exact value.
I will explore speeding up the calculation by excluding some of the vertices from the graph in the pair-wise distance calculation stage and using other vertices to interpolate their distances to other vertices.
There are two ways this thesis will do so, one on both unweighted and weighted graphs and the other only on weighted ones.

Vertices which are linked to only one other vertex can be excluded.
Their distances to all other vertices are simply the distance from the only vertex they are connected to plus the distance to that vertex.

After excluding the vertices only linked to one other vertex, another set of vertices have a trivial distance to all other, vertices which are linked to only two other vertices, the distance to all other vertices is through one of the two.
This method only works well on weighted graphs, because after the exclusion a new edge has to be created with the sum of the two excluded edges, which requires already a weighted graph or a creation of one.

I believe the effect of this exclusion methods can have a large effect on speeding up the calculation, especially in real-world scale-free graphs, where vertices with a degree just one or two form a big part of the graph.
As far as I know, nobody has yet proposed this solution to speed up the average path length.

Then, the approximation of the  average path length.
Ye et al. proposed a random vertex sampling method to approximate the value in~\cite{ye2010distance}, which uses distances from a portion of the vertices to all other vertices to approximate the average.
In the second part of the thesis, I will try to reproduce the results in that paper on other graphs and experiment more broadly with the sampling methods and the size of the set used for the approximation.
I will also closely follow the speed of the approximation with relation to the accuracy to suggest a method to get accurate results very quickly, something the authors did not focus greatly on.

The thesis is structured as follows.
The next chapter defines basic concepts used in the report and their notation.
Chapter~\ref{ch:review} reviews past research into speeding up the calculation and approximations of the average path length and some other properties.
Chapter~\ref{ch:speedup} focuses on the speeding up of the exact calculation and chapter~\ref{ch:approx} focuses on the approximation.
The thesis closes with a conclusion in chapter~\ref{ch:conclusion}.
