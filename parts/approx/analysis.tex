\section{Analysis}\label{sec:approx:analysis}

The paper by Ye at al. compares multiple different sampling methods to approximate the \gls{APL}.
Two are based on sampling either vertices or edges and creating a subgraph, one is a snowball method iteratively adding neighbours to a subgraph and the final one is sampling a set of \textit{source} vertices and calculating distances from only those source vertices.

Out of these, the last one, the method the authors called \textit{random vertex sampling}, works best for approximating the \gls{APL}.
The others have a large bias in over- or underapproximating the value.
The choice was clear, I chose to implement that method, with some modifications and more experiments.

Since I already implemented an algorithm to speed up the calculation, it only makes sense to speed up this approximation with it as well.
The implementation is described in the next section, what most matters is the selection of the source vertices.
I eventually came up with three methods with relation to the algorithm devised in the previous chapter.
Let $p$ denote the parameter for the approximation, which stands for the portion of vertices to be sampled or the \textit{sample size} as it will be referenced in the rest of this thesis.
The parameter will have slightly different meanings in the methods.

Both methods \textbf{\gls{RP}} and \textbf{\gls{RU}} sample $\lceil p \cdot n_r \rceil$ vertices from the reduced graph (graph after removing of 1-core and 2-chains).
Recall that $n_r$ is the number of vertices in that reduced graph.
Let's call those selected vertices \textit{sampled} and let $S$ denote the set.
What differs in those two methods is the probability of selecting each vertex, however, duplicates are not allowed in either.

To calculate the approximation, we have calculate distances on the reduced graph from these vertices.
However thanks to the improved algorithm, we can also count the vertices in 1-core trees connected to vertices in $S$ and if both closest vertices are in $S$ also the 2-chain vertices, with just a couple of extra computations.
Let's call those vertices together with vertices from $S$ \textit{counted} and let $C$ denote the set.

The approximation then is expressed in equation~\ref{eq:ru_apl}.

\begin{equation}\label{eq:ru_apl}
    \bar{l_S} = \frac{\sumop\limits_{i \in C, j \in V, i \neq j} d_{i, j}}{|C| \cdot (|V| - 1)}
\end{equation}

Method \textbf{\acrfull{RP}} samples vertices from the reduced graph based on the number of vertices that would be included in $C$ if it were selected.
The motivation behind is to count as many vertices as possible with the least amount of computation necessary.

The probability of selecting each vertex $v$ is expressed in equation~\ref{eq:rp_prop}.
The vertex itself is always counted, and then if a 1-core tree is connected, all vertices from it can be also counted, as expressed by $n_{1, v}$.
If the graph is weighted, 2-chain vertices can be counted if the other closest vertex is also in the sampled set, so I multiplied the count $n_{2, v}$ with a half, to take in account this relation.
If the graph is not weighted, $n_{2, v}$ is zero.

\begin{equation}\label{eq:rp_prop}
    p_v \propto 1 + n_{1, v} + 0.5 n_{2, v}
\end{equation}

Method \textbf{\acrfull{RU}} samples vertices from the reduced graph uniformly.
As will be shown in the sections to come, method \gls{RP} does not perform well, introducing a significant bias.
Therefore I tried to salvage the method of sampling on the reduced graph with a uniform sampling, which could remove the bias.

Method \textbf{\acrfull{OU}} samples $\lceil p \cdot n \rceil$ vertices from the original graph before any reduction with 1-core and 2-chains.
The sampling is uniform.
In this case, the sampled set $S$ is identical to the counted set $C$, but pair-wise distances on the reduced graph only have to be performed on set $D$.
Set $D$ is a set which contains the closest vertices instead of vertices that are in the 1-core or 2-chains.

Again, as we will see later, method \gls{RU} does not perform very well on weighted graphs -- this is basically the original method introduced in the paper by Ye at al.
However with the improved \gls{APL} algorithm, from a certain number of vertices, the set $D$ will be smaller than $S$ and the calculation quicker.

\subsection{Complexity}\label{subsec:approx:analysis:complexity}

Recall, that the complexity of the \gls{APL} calculation with the improved algorithm is $\bigO(n_r (n_r + m_r) + n_1^2 + n (n_r + 1) + n_1 )$ for unweighted graphs, and $\bigO(n_r m_r + n_r^2 \log n_r + n (n - n_1 + 2) + n_1^2 (2 + \log n_1) + n_2)$ for weighted graphs using the combined algorithm.

The complexity of the pair-wise distance calculation on the reduced graph is $\bigO(n_r (n_r + m_r))$ for unweighted graphs and $\bigO(n_r m_r + n_r^2 \log n_r)$ on weighted graphs.
The complexity of the sum of the distances is the same for both types of graphs, $\bigO(n \cdot n_r + n_1^2)$, just the notation is slightly different for weighted graphs.
The complexities of those two parts are different for the approximation's complexity.

On unweighted graphs, instead of running $n_r$ \glspl{BFS}, only $|S|$ are needed ($|D|$ for method \gls{OU}), putting the complexity at $\bigO(|S| (n_r + m_r))$
On weighted graphs, instead of using Johnson's algorithm, $|S|$ (or $|D|$) separate Dijkstra's algorithms are required, putting the complexity at $\bigO(|S| (m_r + n_r \log n_r))$.

The sum of distances is modified to $\bigO(n \cdot |S| + n^2_1)$ for both types of graphs, this time with the same notation.
For method \gls{OU} the $S$ is replaced with $D$.

One must of course also consider the complexity of the sampling.
Since samples without replacement are required, the easiest solution is to shuffle the options with the Fisher-Yates shuffle, which has complexity $\bigO(n)$~\cite{durstenfeld1964algorithm}, and then take elements from the shuffled list until the required count is needed.
The complexity is then $\bigO(n_r)$ and $\bigO(n)$ for method \gls{OU}.
