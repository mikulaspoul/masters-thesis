\section{Interpretation of results}\label{sec:approx:interpret}

In previous sections I already concluded that method OU is the best for approximating the \gls{APL} -- it is unbiased and its accuracy improves quickly with growing sample size on all graphs with all weight distributions.
Method RU also shows these properties, but only on unweighted graphs.

Now since the method selection is clear, the selection of $p$.
That selection, of course, depends on what the priorities are and there are trade-offs involved.
Selecting a smaller $p$ means a less accurate approximation but a faster calculation.
Higher $p$ provides a more accurate approximation at the cost of more time.
The tables in section~\ref{subsubsec:approx:experiments:result:quant} and figures in section~\ref{subsec:approx:experiments:speed} can help with this trade-off.

To show more of a unified comparison, I put together some overview tables with the full accuracy statistics (mean, standard deviation, minimum and maximum) and the time improvement against the calculation of the exact value.
The tables~\ref{tab:threshold:overview:unweighted},~\ref{tab:threshold:overview:normal} and~\ref{tab:threshold:overview:uniform} show the results for three specific $p$ values -- 0.006, 0.06 and 0.3.
Each table contains results for one type of weights -- no weights, normal weights and uniform weights, unit weights are omitted as the results are similar to no weights at all.

One thing of note is that the improvement against the calculation of the exact value is using the results of the improved algorithm, meaning that if one would compare to the naive algorithm, the improvement would be multiplied by the results in  section~\ref{subsec:speedup:results:improvement}.
We can also see the improvement is not linear with respect to $p$, that is of course because the complexity of the calculation is not linear and there are some computations which must be done regardless of $p$ -- the reduction of the graph.

\begin{table}[htp]
    \centering
    \resizebox{\textwidth}{!}{%
        \input{parts/tables/threshold/overview/unweighted.tex}%
    }
    \captionsetup{justification=centering}
    \caption[Approximation performance for set $p$ for unweighted graphs]{Accuracy and speed of approximation for specific sample sizes for unweighted graphs.
    Time is compared to the calculation of the exact value.}\label{tab:threshold:overview:unweighted}
\end{table}


\begin{table}[htp]
    \centering
    \resizebox{\textwidth}{!}{%
        \input{parts/tables/threshold/overview/normal.tex}%
    }
    \captionsetup{justification=centering}
    \caption[Approximation performance for set $p$ for weighted graphs (normal)]{Accuracy and speed of approximation for specific sample sizes for graphs with normal weights.
    Time is compared to the calculation of the exact value.}\label{tab:threshold:overview:normal}
\end{table}

\begin{table}[htp]
    \centering
%    \resizebox{\textwidth}{!}{%
        \input{parts/tables/threshold/overview/uniform.tex}%
%    }
    \captionsetup{justification=centering}
    \caption[Approximation performance for set $p$ for weighted graphs (uniform)]{Accuracy and speed of approximation for specific sample sizes for graphs with uniform weights.
    Faster columns omitted here to prevent duplicity.}\label{tab:threshold:overview:uniform}
\end{table}

\subsection{Differences between methods}

The differences between methods come from how the graph is reduced by removing the 1-core and 2-chains and how they are counted in the approximation.
As it turns out, when sampling on the reduced graph and counting the extra vertices automatically, a bias is introduced.
If there are many 1-core vertices in the graph, the paths from those vertices to all other vertices are slightly longer on average, resulting in an over-approximation.
The paths from 2-chain vertices are on the other hand shorter than average, resulting in under-approximation.

Method RP suffers from this even further, because vertices which introduce the bias are selected with a higher probability.
The original hypothesis was to count as many vertices as possible with as little computation needed, but from the results, we can see that the result is biased and the difference in speed is not that great to be concerned with.

\subsection{Differences between weight distributions}\label{subsec:approx:interpret:dist}

Comparing weighted and unweighted graphs, it is not surprising that the results for unweighted graphs and unit weights are practically the same, they do have the same distance distribution, and the sampling on the original graph removes any differences in how the value is calculated.
The distribution of the weights seems to differ, the required $p$ is bigger for uniform weights than for normal weights.
In figure~\ref{fig:distance_dist} I plotted the distribution of all the paths within each graph with the weights I generated.

The obvious difference between the uniform and normal weights is that the distances are shifted slightly, which makes the resulting APL different.
But I do not think that is the main reason behind the worse performance of the approximation on uniform weights.
I took a look at the standard deviation and the excess kurtosis of the distributions, shown in tables~\ref{tab:distance_dist_std} and~\ref{tab:distance_dist_kurtosis}.

The standard deviation for unweighted graphs is very small, which is maybe one of the contributing factors why the approximation of unweighted graphs is easier -- a smaller $p$ is required.
The differences in the standard deviation between the two weight distributions go both ways, there are no consistent differences.

But in excess kurtosis, the difference is consistent across the scale-free graphs -- normal weights have a higher kurtosis compared to unweighted graphs and uniform weights have a higher kurtosis compared to normal.
The absolute values do not matter, only the comparison between types of weights.
Higher excess kurtosis means more outlier values, meaning that from the data we see for uniform weights have more outliers and it is, therefore, harder to approximate the mean, because the outliers affect it.
Again, \graph{er-graph} is an exception, the $p$ does not increase dramatically for uniform weights and here we see that the kurtosis is not too different.

I am not saying that is the definite reason, but it is sort of an educated guess.
To actually confirm that more experiments would be required, first of all, generating multiple edge weights in the same distribution, to see how much the result differs if it is not just the result of these specific weights.
Then I think it would be wise to also test with different parameters of the distributions, to see how much the select ones matter.

\begin{figure}[htp]
    \centering
    \captionsetup{justification=centering}

    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.75\linewidth]{figures/distance_dist/as-22july06.pdf}
      \caption{For graph \graph{as-22july06}}
      \label{fig:distance_dist:as-22july06}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.75\linewidth]{figures/distance_dist/cond-mat-2003.pdf}
      \caption{For graph \graph{cond-mat-2003}}
      \label{fig:distance_dist:cond-mat-2003}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.75\linewidth]{figures/distance_dist/ba-graph.pdf}
      \caption{For graph \graph{ba-graph}}
      \label{fig:distance_dist:ba-graph}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.75\linewidth]{figures/distance_dist/er-graph.pdf}
      \caption{For graph \graph{er-graph}}
      \label{fig:distance_dist:er-graph}
    \end{subfigure}

    \caption{Distance distributions of the four test graphs}
    \label{fig:distance_dist}
\end{figure}

\begin{table}[htp]
    \centering
%    \resizebox{\textwidth}{!}{%
        \input{parts/tables/distance_dist_std.tex}%
%    }
    \captionsetup{justification=centering}
    \caption{Standard deviation of the distance distributions}\label{tab:distance_dist_std}
\end{table}


\begin{table}[htp]
    \centering
%    \resizebox{\textwidth}{!}{%
        \input{parts/tables/distance_dist_kurtosis.tex}%
%    }
    \captionsetup{justification=centering}
    \caption{Excess kurtosis of the distance distributions}\label{tab:distance_dist_kurtosis}
\end{table}
