\subsection{Experiments results}\label{subsec:approx:experiments:result}

The full experiment results can be seen plotted in appendix~\ref{ch:appendix:approx}.
Since there is a lot of combinations of graphs, methods and types of weights, there is a lot of individual plots, so I am only going to include a couple of them here as examples on the `\textit{neutral}' scale-free \graph{ba-graph}.
For the rest of the graphs, I will only include the quantitative results with thresholds.

Figure~\ref{fig:sampling_example:unweighted} shows the results of experiments on the unweighted \graph{ba-graph} with the three methods.
After 40 runs of the experiments, we can see that on unweighted graphs methods RU and OU are unbiased, the mean is very close to the true value even for very small $p$.
The method RP is slightly biased, producing a value above the true value of the property.
The standard deviation of the RP method is also larger than for the other two, which seem to have comparable standard deviations.
The extremes of the approximation are also shown in the plots.

\begin{figure}[htp]
    \centering
    \captionsetup{justification=centering}

    \begin{subfigure}{.333\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/sampling/unweighted/absolute_ba-graph_unweighted_rp.pdf}
      \caption{Method RP}
      \label{fig:sampling_example:unweighted:reduced_prob}
    \end{subfigure}%
    \begin{subfigure}{.333\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/sampling/unweighted/absolute_ba-graph_unweighted_ru.pdf}
      \caption{Method RU}
      \label{fig:sampling_example:unweighted:reduced_uniform}
    \end{subfigure}%
    \begin{subfigure}{.333\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/sampling/unweighted/absolute_ba-graph_unweighted_ou.pdf}
      \caption{Method OU}
      \label{fig:sampling_example:unweighted:full}
    \end{subfigure}

    \caption[Approximation results on unweighted \graph{ba-graph}]{Approximation results on the unweighted \graph{ba-graph}.
             Note the logarithmic scale on the X-axis.}
    \label{fig:sampling_example:unweighted}
\end{figure}

Figure~\ref{fig:sampling_example:weighted:unit} shows the results of experiments on the \graph{ba-graph} with unit weights.
After 20 runs, the differences between the methods are more apparent here than in the unweighted variants.
There is now a bias in both methods RU and RP, usually under approximating the value, but in RP for high values of $p$, it is also overapproximating the value.
Method OU remains unbiased and its standard deviation and extremes are better compared to the other methods.

\begin{figure}[htp]
    \centering
    \captionsetup{justification=centering}

    \begin{subfigure}{.333\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/sampling/weighted/unit/absolute_ba-graph_weighted_unit_rp.pdf}
      \caption{Method RP}
      \label{fig:sampling_example:weighted:unit:reduced_prob}
    \end{subfigure}%
    \begin{subfigure}{.333\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/sampling/weighted/unit/absolute_ba-graph_weighted_unit_ru.pdf}
      \caption{Method RU}
      \label{fig:sampling_example:weighted:unit:reduced_uniform}
    \end{subfigure}%
    \begin{subfigure}{.333\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/sampling/weighted/unit/absolute_ba-graph_weighted_unit_ou.pdf}
      \caption{Method OU}
      \label{fig:sampling_example:weighted:unit:full}
    \end{subfigure}

    \caption[Approximation results on weighted \graph{ba-graph} with unit weights]{Approximation results on the weighted \graph{ba-graph} with unit weights}
    \label{fig:sampling_example:weighted:unit}
\end{figure}

With other weight distributions for this graph and the three other graphs, the overall trend is very similar.
For some graphs and some weights the direction and size of the bias are different with methods RP and RU, but the method OU is always unbiased.
This can be seen in the figures in appendix~\ref{ch:appendix:approx}.

\subsubsection{Quantitative threshold results}\label{subsubsec:approx:experiments:result:quant}

From the plots in the previous section, we can already see that method OU performs the best both on unweighted and weighted graphs.
So here in this section, I will only include its threshold results, but the results for all methods are included in the appendix~\ref{ch:appendix:approx_tables}.
Furthermore, as can be seen from the results in appendix~\ref{ch:appendix:approx_tables}, the thresholds for unit weights are practically identical to unweighted graphs, so I omitted those results from this section as well.
That is because the distances are the same and method OU is not dependent on the effect of the reduction.

Table~\ref{tab:threshold:just_ou:mean} shows the threshold results on mean error for method OU on unweighted graphs and weighted graphs with normal and uniform weights.
On unweighted graphs, we can see that the 5\% threshold can be reached with $p$ on the $10^{-4}$ scale and then each further threshold is reached with an increase of an order of magnitude of $p$.
The \graph{er-graph} is an exception to that, about an order of magnitude smaller $p$ is required for the thresholds.

On normally distributed weights, the performance of the approximation drops in all graphs, but not significantly for most thresholds.
The only threshold where the increase is significant is 0.1\%, where it raises the sample size up to 0.5, half of the whole graph has to be used as a source.

On uniformly distributed weights the performance drops again compared to normal weights.
For thresholds higher than or equal to 0.5\% the approximation is still a viable option that will make the calculation quicker, but for 0.1\% $p$ raises up to 0.85, practically using the whole graph.

\begin{table}[htp]
    \centering
%    \resizebox{\textwidth}{!}{%
        \input{parts/tables/threshold/overview/ou_mean.tex}%
%    }
    \captionsetup{justification=centering}
    \caption[Threshold results with method OU (mean error)]{Threshold results with method OU on unweighted graphs (UW), weighted graphs with normal weights (WN) and uniform weights (WU)}\label{tab:threshold:just_ou:mean}
\end{table}

Table~\ref{tab:threshold:just_ou:mean} shows the threshold results on the maximum error.
We can again see a general rule that each threshold is reached with an increase in the magnitude of $p$ and that maximum 1\% error is reached with $p$ on the $10^{-2}$ scale, single percentages of the graph used as sources.
As in the mean error, the approximation is not as performant on the uniformly distributed weights, requiring almost an order of magnitude larger $p$ than other weight distributions, however, $p = 0.2$ still always results in an error less than 1\%.
I will try to explore why the approximation performs worse on uniform weights in section~\ref{subsec:approx:interpret:dist}.

\begin{table}[htp]
    \centering
    \resizebox{\textwidth}{!}{%
        \input{parts/tables/threshold/overview/ou_max.tex}%
    }
    \captionsetup{justification=centering}
    \caption[Threshold results with method OU (maximum error)]{Threshold results with method OU on unweighted graphs (UW), weighted graphs with normal weights (WN) and uniform weights (WU)}\label{tab:threshold:just_ou:max}
\end{table}

\subsubsection{Numbers of vertices sampled or counted}\label{subsubsec:approx:experiments:result:counted}

As mentioned in the section about the design of experiments, I was also tracking how many vertices were sampled, counted with methods RU and RP and for how many vertices the distances had to be calculated in method OU\@.

Figure~\ref{fig:sampling_counted:unweighted} contains the results on unweighted graphs, averages of 40 runs.
The size of $C$ for method RU is almost identical to the size of $S$ for method OU, so it is practically invisible.
The key finding is that the size of $D$ in method OU grows similarly to the size of $S$ in the other methods, so time-wise they should perform similarly, as the same number of distances need to be calculated for all methods.

For each graph we can also see how many of the vertices are in the 1-core or 2-chains in the differences between the size of $S$ and $C$ for methods RU and RP.
For example, \graph{er-graph} is not scale-free and therefore the difference is small.

\begin{figure}[htp]
    \centering
    \captionsetup{justification=centering}

    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/as-22july06.pdf}
      \caption{For graph \graph{as-22july06}}
      \label{fig:sampling_counted:unweighted:as-22july06}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/cond-mat-2003.pdf}
      \caption{For graph \graph{cond-mat-2003}}
      \label{fig:sampling_counted:unweighted:cond-mat-2003}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/ba-graph.pdf}
      \caption{For graph \graph{ba-graph}}
      \label{fig:sampling_counted:unweighted:ba-graph}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/er-graph.pdf}
      \caption{For graph \graph{er-graph}}
      \label{fig:sampling_counted:unweighted:er-graph}
    \end{subfigure}

    \caption[Number of vertices sampled and counted on unweighted graphs]{Number of vertices counted and sampled for the three methods with respect to the sample size on unweighted graphs}
    \label{fig:sampling_counted:unweighted}
\end{figure}

Figure~\ref{fig:sampling_counted:weighted} contains the results on weighted graphs, combined for all distributions of weights.
Here the differences between methods are even more apparent, especially on \graph{as-22july06}.
The 1-core and 2-chains vertices are a big part of that graph, which results in a very quick increase of counted vertices in method RP, as a few vertices can provide results for many vertices.
The difference in the size of $D$ in OU and $S$ in RU/RP is larger on weighted graphs, but not as significant to be worried about, especially since it provides such a stronger approximation.

\begin{figure}[htp]
    \centering
    \captionsetup{justification=centering}

    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/as-22july06_weighted.pdf}
      \caption{For graph \graph{as-22july06}}
      \label{fig:sampling_counted:weighted:as-22july06}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/cond-mat-2003_weighted.pdf}
      \caption{For graph \graph{cond-mat-2003}}
      \label{fig:sampling_counted:weighted:cond-mat-2003}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/ba-graph_weighted.pdf}
      \caption{For graph \graph{ba-graph}}
      \label{fig:sampling_counted:weighted:ba-graph}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
      \centering
      \includegraphics[width=0.9\linewidth]{figures/sampling/counted/er-graph_weighted.pdf}
      \caption{For graph \graph{er-graph}}
      \label{fig:sampling_counted:weighted:er-graph}
    \end{subfigure}

    \caption[Number of vertices sampled and counted on weighted graphs]{Number of vertices counted and sampled for the three methods with respect to the sample size on weighted graphs}
    \label{fig:sampling_counted:weighted}
\end{figure}
