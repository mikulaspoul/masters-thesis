\chapter{Conclusion}\label{ch:conclusion}
\glsresetall

This thesis set out two goals -- to speed up the exact calculation of the \gls{APL} and then use that algorithm to provide an even quicker and accurate approximation.
I think I succeeded in both.

I used two tricks to speed up the calculation, both based on reducing the number of vertices and edges in the most expensive part of the calculation, the pair-wise distance calculation within a graph, and calculating the distances to those vertices in alternative ways.
The first trick which works on all graphs is removing the 1-core from the graph -- the path from a vertex in a 1-core always goes through one specific vertex and the distance from that vertex can be used.
The second trick only works on weighted graphs is removing vertices which have a degree two -- the paths from those vertices always go through either one or the other connected vertex.

Both of these tricks reduce the size of the graph for the expensive pair-wise distance calculation significantly mainly on scale-free graphs, which is the class of most real-world graphs.
These graphs follow a power-law degree distribution, meaning there is a high portion of vertices in the graph which have a very low degree.
The improved algorithm has both a better computational complexity and better space complexity.

After implementing the algorithm in C++ with Python bindings I tested the algorithm on four large graphs, a graph of \glspl{AS}, a citation network, a \gls{BA} and \gls{ER} random graphs, ranging in size from 22k to 30k vertices.
When comparing the time needed to calculate the APL I found that the improved algorithm sped up the calculation up to 2.2 times, with the specific time depending on the size of the 1-core and the overall number of vertices and edges in the graph.

When considering weighted graphs, the improved algorithm sped up the calculation up to 8.6 times when real time was measured and 3.3 times when CPU time was measured.
Apart from reducing the size of a graph for pair-wise distance calculation my improved algorithm also enables more of parallelisation.

The algorithm I devised can be generalised to be applied to any pair-wise distance problem, but in this thesis, it does focus on the average path length.
It can be used to speed up the creation of the 2D distance matrix for graphs and therefore all problems that depend on it.
It could even be used for example for calculation of betweenness, where the number of paths is important as well, with some modifications.
The core principle is that the paths from some vertices are trivial with respect to some other vertex and the whole graph does not have to be explored to have the same information.

In the second part of the thesis, I implemented an approximation on top of the improved algorithm.
The approximation selects several vertices and only counts distances from those vertices instead from all.
This method was already published in 2010 and I mostly experimented with new methods of selecting the vertices, with relation to the improved algorithm.
I was hoping that one of the methods I would propose would do better, but they introduced a bias to the approximation instead.
So while the idea behind the method remains unchanged in this thesis I further sped it up with my improved \gls{APL} algorithm, which improves both the computational complexity and space complexity.

I used the same graphs and generated multiple weights distributions and did more analysis on the sample sizes and resulting accuracy and I measured how much quicker it is compared to the calculation of the exact value.
The speedup and accuracy of the method are satisfactory, even with very small sample sizes the maximum error rarely exceeds 5\% and it is multiple times to hundreds of times faster.
The speed and the resulting accuracy, of course, form a trade-off, the bigger the sample size the more accurate the approximation is while taking more time to calculate.

To summarise my contributions, I devised and implemented a faster exact calculation of the average path length on scale-free graphs with moderate to good improvements in speed.
Then I reproduced results of a paper on approximations of the average path length using sampling, improved its speed, experimented with more methods of the sampling, and mainly experimented with more different sampling sizes and measured both the accuracy and time improvement, to provide a trade-off based on priorities placed on the approximation.
